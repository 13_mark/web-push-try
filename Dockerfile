FROM node:18-bullseye

WORKDIR /usr/web-push-try
COPY package.json /usr/web-push-try/

RUN npm install
COPY ./ /usr/web-push-try

CMD ["sh", "-c", "node src/server/index.js"]