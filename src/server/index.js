const webpush = require('web-push');
const express = require('express');

const publicVapidKey = 'BLW9keYEisr4Ejww65FsPMFZN59bEttxaHqqdVx4h7s8z3vRiAZLCE_JAAB6LTcu9jpvzYldHLw2Bx_IZmZFndI';
const privateVapidKey = '9p2ScbeRspYJWU1aVRHcmSlFenZKZDfSDBWCjyAUqLg';

let playerSubscriptionLookup = {}

webpush.setVapidDetails('mailto:13_mark@protonmail.com', publicVapidKey, privateVapidKey);

const app = express();

app.use(require('body-parser').json());

app.post('/register', (req, res) => {
  const { subscription, players } = req.body;

  playerSubscriptionLookup = players.reduce((acc, player) => {
    return {
      ...acc,
      [player]: [
        ...(playerSubscriptionLookup[player] || []),
        subscription
      ]
    }
  }, playerSubscriptionLookup)

  res.status(200).json({ message: 'success', playerSubscriptionLookup })
})

app.post('/notify', async (req, res) => {
  const { home, away, homeScore, awayScore } = req.body;

  const payload = { home, away, homeScore, awayScore };

  const homeSubscriptions = playerSubscriptionLookup[home] || []
  const awaySubscriptions = playerSubscriptionLookup[away] || []

  const results = []

  for (const homeSubscription of homeSubscriptions) {
    const result = await webpush.sendNotification(homeSubscription, JSON.stringify({ ...payload, playerToNotifyFor: home })).catch(error => {
      console.error(error.stack);
    });

    results.push(result)
  }

  for (const awaySubscription of awaySubscriptions) {
    const result = await webpush.sendNotification(awaySubscription, JSON.stringify({ ...payload, playerToNotifyFor: away })).catch(error => {
      console.error(error.stack);
    });

    results.push(result)
  }

  res.status(200).json(results.length)
})


app.use(require('express-static')('./src/client'));

app.listen(3000);