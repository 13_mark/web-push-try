var button = document.getElementById('send-button')
var homeInput = document.getElementById('home')
var awayInput = document.getElementById('away')
var homeScoreInput = document.getElementById('homeScore')
var awayScoreInput = document.getElementById('awayScore')

var progressIndicator = document.getElementById('loading-indicator');

async function sendNotification() {

    startProgressBar();

    var home = homeInput.value
    var away = awayInput.value
    var homeScore = homeScoreInput.value
    var awayScore = awayScoreInput.value

    await fetch('/notify', {
        method: 'POST',
        body: JSON.stringify({
            home,
            away,
            homeScore,
            awayScore
        }),
        headers: {
            'content-type': 'application/json'
        }
    })

    stopProgressBar()
    showSuccessMessage()
    resetFormFields()
}

function startProgressBar() {
    progressIndicator.style = '';
}

function stopProgressBar() {
    progressIndicator.style = 'display: none;'
}

function showSuccessMessage() {
    alert('Benachrichtigung wurde erfolgreich abgeschickt')
}

function resetFormFields() {
    homeInput.value = ''
    awayInput.value = ''
    homeScoreInput.value = ''
    awayScoreInput.value = ''
}