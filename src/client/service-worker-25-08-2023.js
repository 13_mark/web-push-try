self.addEventListener('push', ev => {
  const {home, away, homeScore, awayScore, playerToNotifyFor} = ev.data.json();
  self.registration.showNotification(`Neues Ergebniss für ${playerToNotifyFor}`, {
    body: `${home} ${homeScore} : ${awayScore} ${away}`,
    icon: './favicon.ico'
  });
});