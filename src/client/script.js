const publicVapidKey = 'BLW9keYEisr4Ejww65FsPMFZN59bEttxaHqqdVx4h7s8z3vRiAZLCE_JAAB6LTcu9jpvzYldHLw2Bx_IZmZFndI';

const notifyAllInfoElement = document.getElementById('notify-all-info')
const errorInfoElement = document.getElementById('error-info')
const loadingElement = document.getElementById('loading-indicator')

const prerequisiteSuccessIcon = document.getElementById('prerequisite-success-icon')
const permissionSuccessIcon = document.getElementById('permission-success-icon')

const playerSelectionArea = document.getElementById('player-selection-area')

const initialisationErrorArea = document.getElementById('initialisation-error-area');
const errorDetails = document.getElementById('error-details')

const userCardsArea = document.getElementById('user-cards-area')

const submitProgressBar = document.getElementById('submit-progress-bar')
const finalSuccessArea = document.getElementById('final-success-area')

var serviceWorkerRegistration;
var allPlayers;


init().then(
    () => loadingElement.classList.add('is-hidden')
)

async function init() {
    try {
        allPlayers = await readAllPlayerFromFile();
        updateUserCardsArea(allPlayers)
        document.getElementById('search').value = ''

        await checkPrerequisites();
        serviceWorkerRegistration = await registerServiceWorker();
        await askPermission()
        playerSelectionArea.classList = playerSelectionArea.classList.remove('is-hidden')
    } catch (error) {
        initialisationErrorArea.classList = initialisationErrorArea.classList.remove('is-hidden')
        errorDetails.innerHTML = error;
    }
}

async function checkPrerequisites() {
    return new Promise((resolve, reject) => {
        const serviceWorkerAvailable = 'serviceWorker' in navigator
        const pushManagerAvailable = 'PushManager' in window

        if (!!serviceWorkerAvailable && !!pushManagerAvailable) {
            setTimeout(() => {
                prerequisiteSuccessIcon.classList = prerequisiteSuccessIcon.classList.remove('is-hidden')
                resolve()
            }, 2000)
        } else {
            reject(`Prerequisites not met. Service Worker: ${!!serviceWorkerAvailable} | Push Manager: ${!!pushManagerAvailable}`)
        }
    })

}

function registerServiceWorker() {
    return navigator.serviceWorker.register('service-worker-25-08-2023.js');
}

async function askPermission() {
    const askPermissionPromise = new Promise(function (resolve, reject) {
        const permissionResult = Notification.requestPermission(function (result) {
            resolve(result);
        });

        if (permissionResult) {
            permissionResult.then(resolve, reject);
        }
    });

    const result = await askPermissionPromise;

    if (result === 'granted') {
        permissionSuccessIcon.classList = permissionSuccessIcon.classList.remove('is-hidden')

        return;
    } else {
        throw new Error('Permission was not granted')
    }

}



function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

async function readAllPlayerFromFile() {
    return fetch("https://player-admin-backend.cratory.de/players")
        .then((res) => res.json())
        .then((players) => players.map((player) => ({ name: player.name, selected: false }))
                                  .sort((a, b) => a.name > b.name ? 1 : -1)
                                  .reduce(
            (acc, curr) => {
                return {
                    ...acc,
                    [curr.name]: curr
                }
            }, {}
        ))
        .catch((e) => console.error(e));
}

function updateUserCardsArea(playersInput) {
    const perChunk = 4;

    const playersAsArray = Object.values(playersInput)

    const result = playersAsArray.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / perChunk)

        if (!resultArray[chunkIndex]) {
            resultArray[chunkIndex] = []
        }

        resultArray[chunkIndex].push(item)

        return resultArray
    }, [])

    const resultHTML = result.reduce((acc, curr) => {
        return `
            ${acc}
            <div class="row">
                ${curr.reduce((acc, { name, selected }) => `
                ${acc}
                <div class="card col user-card ${selected ? 'selected' : ''}" onclick="onPlayerSelected('${name}')">
                    ${name}
                </div>
                ` , '')}
            </div>
        `
    }, '');

    userCardsArea.innerHTML = resultHTML
}

function onPlayerSelected(player) {
    allPlayers = {
        ...allPlayers,
        [player]: {
            name: player,
            selected: true
        }
    }
    document.getElementById('search').value = ''
    updateUserCardsArea(allPlayers)
}

async function submitPlayerList() {
    const players = Object.values(allPlayers).filter((player) => player.selected).map((player) => player.name)

    const pushManagerSubscription = await serviceWorkerRegistration.pushManager.
        subscribe({
            userVisibleOnly: true,
            applicationServerKey: urlBase64ToUint8Array(publicVapidKey)
        });

    submitProgressBar.classList = submitProgressBar.classList.remove('is-hidden')

    await fetch('/register', {
        method: 'POST',
        body: JSON.stringify({
            subscription: pushManagerSubscription,
            players
        }),
        headers: {
            'content-type': 'application/json'
        }
    })

    playerSelectionArea.classList.add('is-hidden')
    finalSuccessArea.classList = finalSuccessArea.classList.remove('is-hidden')
    document.getElementById('result-list').innerHTML = players.reduce((acc, curr) => `
        ${acc}
        <li>${curr}</li>
    `, '')
}

function onSearchInputChanged() {
    const value = document.getElementById('search').value;

    if (!!value) {

        const regex = new RegExp(`(.*${value}*)`, 'i')
        const players = Object.values(allPlayers)
        const filteredPlayers = players.filter(({ name }) => !!name.match(regex, 'i'))

        updateUserCardsArea(filteredPlayers)
    } else {
        updateUserCardsArea(allPlayers)
    }

}